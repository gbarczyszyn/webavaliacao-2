/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import if6ae.entity.Inscricao;
import if6ae.entity.InscricaoMinicurso;
import if6ae.entity.InscricaoMinicurso_;
import if6ae.entity.Inscricao_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

/**
 *
 * @author guilherme
 */
public class InscricaoJpa extends Jpa {

    public List<Inscricao> findAll() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
            cq.from(Inscricao.class);
            TypedQuery<Inscricao> q = em.createQuery(cq);

            return q.getResultList();
        } catch (Exception e) {
            System.err.println("Erro ao acesssar banco de dados: " + e.getLocalizedMessage());
        } finally {
            em.close();
        }

        return null;
    }

    public List<Inscricao> findAllMinicursos() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
            Root<Inscricao> rt = cq.from(Inscricao.class);
            Join<Inscricao, InscricaoMinicurso> join = rt.join(Inscricao_.inscricaoMinicursoCollection);
            cq.select(join.get(InscricaoMinicurso_.inscricao));
            TypedQuery<Inscricao> q = em.createQuery(cq);

            return q.getResultList();
        } catch (Exception e) {
            System.err.println("Erro ao acesssar banco de dados: " + e.getLocalizedMessage());
        } finally {
            em.close();
        }

        return null;
    }

    public Inscricao findByNumero(Integer numero) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
            Root<Inscricao> rt = cq.from(Inscricao.class);
            cq.where(cb.equal(rt.get(Inscricao_.numero), numero));
            TypedQuery<Inscricao> q = em.createQuery(cq);

            return q.getSingleResult();

        } catch (Exception e) {
            System.err.println("Erro ao acesssar banco de dados: " + e.getLocalizedMessage());
        } finally {
            em.close();
        }

        return null;

    }

    public Inscricao findByCpf(Long cpf) {
        EntityManager em = getEntityManager();

        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
            Root<Inscricao> rt = cq.from(Inscricao.class);
            cq.where(cb.equal(rt.get(Inscricao_.cpf), cpf));
            TypedQuery<Inscricao> q = em.createQuery(cq);

            return q.getSingleResult();
        } catch (Exception e) {
            System.err.println("Erro ao acesssar banco de dados: " + e.getLocalizedMessage());
        } finally {
            em.close();
        }

        return null;
    }
}
