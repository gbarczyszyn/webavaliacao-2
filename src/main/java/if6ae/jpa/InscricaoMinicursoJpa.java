/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import if6ae.entity.Inscricao;
import if6ae.entity.InscricaoMinicurso;
import if6ae.entity.InscricaoMinicurso_;
import if6ae.entity.Inscricao_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

/**
 *
 * @author guilherme
 */
public class InscricaoMinicursoJpa extends Jpa {

    public InscricaoMinicursoJpa() {

    }

    public List<InscricaoMinicurso> findAll() {
        EntityManager em = getEntityManager();

        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<InscricaoMinicurso> cq = cb.createQuery(InscricaoMinicurso.class);
            cq.from(InscricaoMinicurso.class);
            TypedQuery<InscricaoMinicurso> q = em.createQuery(cq);

            return q.getResultList();
        } catch (Exception e) {
            System.err.println("Erro: " + e.getLocalizedMessage());
        } finally {
            em.close();
        }

        return null;
    }

    public List<InscricaoMinicurso> findInscricaoMinicursoByNumero(Integer numero) {
        EntityManager em = getEntityManager();

        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<InscricaoMinicurso> cq = cb.createQuery(InscricaoMinicurso.class);
            Root<InscricaoMinicurso> rt = cq.from(InscricaoMinicurso.class);
            // Join com inscrição, infos de inscrição vão todos para a variável joinInsricao, a partir daí podemos fazer where
            Join<InscricaoMinicurso, Inscricao> joinInscricao = rt.join(InscricaoMinicurso_.inscricao);
            cq.where(cb.equal(joinInscricao.get(Inscricao_.numero), numero));
            TypedQuery<InscricaoMinicurso> q = em.createQuery(cq);

            return q.getResultList();
        } catch (Exception e) {
            System.err.println("Erro: " + e.getLocalizedMessage());
        } finally {
            em.close();
        }

        return null;
    }
}
