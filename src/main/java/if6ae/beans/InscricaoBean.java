/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.beans;

import if6ae.entity.Inscricao;
import if6ae.jpa.InscricaoJpa;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import utfpr.faces.support.PageBean;

/**
 *
 * @author guilherme
 */
@ManagedBean
@RequestScoped
public class InscricaoBean extends PageBean {
    public List<Inscricao> getInscricoes() {
        InscricaoJpa jpa = new InscricaoJpa();

        return jpa.findAll();
    }
}
